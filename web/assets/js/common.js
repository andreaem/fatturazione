$(document).ready(function() {
    $('#sb-search-button').click(function (e) {
        e.preventDefault();
        if($('.sb-search-input').hasClass('sb-search-hidden')) {
            $('.sb-search-input')
                .removeClass('sb-search-hidden')
                .addClass('sb-search-visible')
                .animate({width: '100%'});
        } else {
            $('.sb-search-input')
                .animate({width: '0%'},'100','',hideSB);
        }
        function hideSB () {
            $('.sb-search-input')
                .removeClass('sb-search-visible')
                .addClass('sb-search-hidden')
        }
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
});
function showNotify(title,message,icon,type) {
    $.notify({
        title: title,
        message: message,
        icon: icon
    },{
        type: type,
        allow_dismiss: false,
        placement: {
            from: "top",
            align: "right"
        },
        delay: 500,
        showProgressbar: false
    });
}