
FROM ubuntu:zesty

LABEL Name=Fatturazione Version=0.0.1 

RUN apt-get -y update && apt-get install -y fortunes

RUN mkdir /var/www/html
WORKDIR /var/www/html

COPY . .

CMD ["printenv"]