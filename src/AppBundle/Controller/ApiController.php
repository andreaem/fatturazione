<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Notes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

class ApiController extends Controller
{
    /**
     * @Route("api/version_check/",name="api_versioncheck")
     */
    public function apiVersionCheck() {
        return new Response('0.1.2');
    }

    /**
     * @Route("api/plugins_control/{action}/{plugin}", name="api_plugincontrol_action")
     */
    public function pluginControlAction($action,$plugin) {
        $settingsFile = $this->getParameter('kernel.project_dir'). DIRECTORY_SEPARATOR .'app/config/app.yml';
        if($action == 'activate') {
            switch ($plugin) {
                case 'calendar':
                    $yaml = Yaml::parse(file_get_contents($settingsFile));
                    $yaml['settings']['sections_enabled']['calendar'] = true;
                    $new_yaml = Yaml::dump($yaml, 5);
                    file_put_contents($this->container->get('kernel')->getRootDir() .'/config/app.yml', $new_yaml);
                    return new Response('Calendar Activated');
                    break;
                case 'time_tracker':
                    $yaml = Yaml::parse(file_get_contents($settingsFile));
                    $yaml['settings']['sections_enabled']['time_tracker'] = true;
                    $new_yaml = Yaml::dump($yaml, 5);
                    file_put_contents($this->container->get('kernel')->getRootDir() .'/config/app.yml', $new_yaml);
                    return new Response('Time Tracker Activated');
                    break;
            }
        } elseif ($action == 'deactivate') {
            switch ($plugin) {
                case 'calendar':
                    $yaml = Yaml::parse(file_get_contents($settingsFile));
                    $yaml['settings']['sections_enabled']['calendar'] = false;
                    $new_yaml = Yaml::dump($yaml, 5);
                    file_put_contents($this->container->get('kernel')->getRootDir() .'/config/app.yml', $new_yaml);
                    return new Response('Calendar Deactivated');
                    break;
                case 'time_tracker':
                    $yaml = Yaml::parse(file_get_contents($settingsFile));
                    $yaml['settings']['sections_enabled']['time_tracker'] = false;
                    $new_yaml = Yaml::dump($yaml, 5);
                    file_put_contents($this->container->get('kernel')->getRootDir() .'/config/app.yml', $new_yaml);
                    return new Response('Time Tracker Dectivated');
                    break;
            }
        }
    }

    /**
     * @Route("api/submit_settings/",name="api_submitsettings")
     */
    public function apiSubmitSettings() {
        $response = var_dump($_GET);

        $settingsFile = $this->getParameter('kernel.project_dir'). DIRECTORY_SEPARATOR .'app/config/app.yml';
        $yaml = Yaml::parse(file_get_contents($settingsFile));
        $yaml['app']['language'] = $_GET['language'];
        $yaml['app']['vat'] = $_GET['vat'];
        $yaml['app']['currency'] = $_GET['currency'];
        $yaml['company']['name'] = $_GET['name'];
        $yaml['company']['address'] = $_GET['address'];
        $yaml['company']['cap'] = $_GET['cap'];
        $yaml['company']['city'] = $_GET['city'];
        $yaml['company']['prov'] = $_GET['prov'];
        $yaml['company']['cf'] = $_GET['cf'];
        $yaml['company']['vat_n'] = $_GET['vat_n'];
        $yaml['company']['mail'] = $_GET['email'];
        $yaml['company']['phone'] = $_GET['phone'];
        $new_yaml = Yaml::dump($yaml, 5);
        file_put_contents($this->container->get('kernel')->getRootDir() .'/config/app.yml', $new_yaml);

        return new Response($response);
    }

    /**
     * @Route("api/load_note/{id}",name="api_loadnote")
     */
    public function apiLoadNote($id) {

        $repo = $this->getDoctrine()->getRepository(Notes::class);
        $note = $repo->findBy(array('id'=>$id));

        return new Response($note[0]->getText());
    }

    /**
     * @Route("api/dave_note/{id}",name="api_savenote")
     */
    public function apiSaveNote($id) {

        $repo = $this->getDoctrine()->getRepository(Notes::class);
        $note = $repo->findBy(array('id'=>$id));

        $note->setText($_POST['text']);

        return new Response($note[0]->getText());
    }
}
