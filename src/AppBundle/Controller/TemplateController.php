<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class TemplateController extends Controller
{
    /**
     * @Route(name="header")
     */
    public function generateHeaderAction() {

    }
    /**
     * @Route(name="sidebar")
     */
    public function generateSidebarAction() {

        $settingsFile = $this->getParameter('kernel.project_dir'). DIRECTORY_SEPARATOR .'app/config/app.yml';
        try {
            $value = Yaml::parse(file_get_contents($settingsFile));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
            $value = null;
        }

        return $this->render('template/sidebar.html.twig', array(
            'plugin_time_tracker' => $value['settings']['sections_enabled']['time_tracker'],
            'plugin_calendar' => $value['settings']['sections_enabled']['calendar']
        ));
    }
}
