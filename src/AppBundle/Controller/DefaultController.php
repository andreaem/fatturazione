<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Clients;
use AppBundle\Entity\Expenses;
use AppBundle\Entity\Invoices;
use AppBundle\Entity\Notes;
use AppBundle\Entity\Products;
use AppBundle\Entity\TimeTracker;
use AppBundle\Repository\InvoicesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $earnings = $this->getDoctrine()->getManager()->getRepository(Invoices::class);
        $earningsYear = $this->getDoctrine()->getRepository(Invoices::class)->createQueryBuilder('e')->select('SUM(e.total) as totals')->where('e.status = 1')->getQuery()->getOneOrNullResult();
        if (empty($earningsYear['totals']) == true) { $earningsYear = array("totals"=> 0); }
        $earningsExpect = $this->getDoctrine()->getRepository(Invoices::class)->createQueryBuilder('e')->select('SUM(e.total) as totals')->where('e.status = 0 OR e.status = 1 OR e.status = 2 OR e.status = 3')->getQuery()->getOneOrNullResult();
        if (empty($earningsExpect['totals']) == true) { $earningsExpect = array("totals"=> 0); }
        $expenses = $this->getDoctrine()->getManager()->getRepository(Expenses::class);
        $expensesAll = $expenses->findAll();
        $expensesTotal = $this->getDoctrine()->getRepository(Expenses::class)->createQueryBuilder('e')->select('SUM(e.total) as totals')->where('e.paid = 1')->getQuery()->getOneOrNullResult();
        $invoices = $this->getDoctrine()->getManager()->getRepository(Invoices::class)->findAll();
        $expensesExpect = $this->getDoctrine()->getRepository(Expenses::class)->createQueryBuilder('e')->select('SUM(e.total) as totals')->getQuery()->getOneOrNullResult();;

        setlocale (LC_TIME, "it_IT");
        $months = array('1' => strftime("%B", strtotime("-3 months")),
                        '2' => strftime("%B", strtotime("-2 months")),
                        '3' => strftime("%B", strtotime("-1 months")),
                        '4' => strftime("%B"),
                        '5' => strftime("%B", strtotime("+1 months")));

        $earningsMonth = array( '1' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("-3 months")),1),
                                '2' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("-2 months")),1),
                                '3' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("-1 months")),1),
                                '4' => $this->getDataByMonth($earnings,date("Y"), strftime("%m"),1),
                                '5' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("+1 months")),1));

        $earningsExpectMonth = array( '1' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("-3 months"))),
                                      '2' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("-2 months"))),
                                      '3' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("-1 months"))),
                                      '4' => $this->getDataByMonth($earnings,date("Y"), strftime("%m")),
                                      '5' => $this->getDataByMonth($earnings,date("Y"), strftime("%m", strtotime("+1 months"))));

        $expensesMonth = array( '1' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("-3 months")),1),
            '2' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("-2 months")),1),
            '3' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("-1 months")),1),
            '4' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m"),1),
            '5' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("+1 months")),1));

        $expensesExpectMonth = array( '1' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("-3 months"))),
            '2' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("-2 months"))),
            '3' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("-1 months"))),
            '4' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m")),
            '5' => $this->getExpenseByMonth($expenses,date("Y"), strftime("%m", strtotime("+1 months"))));

        return $this->render('default/dashboard.html.twig', [
            'page_name' => 'Dashboard',
            'earningsYear' => $earningsYear,
            'earningsMonth' => $earningsMonth,
            'earningsExpect' => $earningsExpect,
            'earningsExpectMonth' => $earningsExpectMonth,
            'expenses' => $expensesAll,
            'expensesMonth' => $expensesMonth,
            'expensesExpectMonth' => $expensesExpectMonth,
            'expensesTotal' => $expensesTotal,
            'expensesExpect' => $expensesExpect,
            'invoices' => $invoices,
            'months' => $months,
            'functions' => $this,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("timetracker/", name="timetracker")
     */
    public function timetrackerAction() {
        setlocale(LC_TIME, 'it_IT');
        date_default_timezone_set('Europe/Rome');
        $now = new \DateTime();

        $items = $this->getDoctrine()->getRepository(TimeTracker::class)->findAll();

        $form = $this->createFormBuilder( new TimeTracker())
            ->add('timeStart', DateTimeType::class, array('widget'=> 'single_text', 'label' => 'Orario avvio',
                'attr' => array('class' => 'form-control', 'disabled'=> 'disabled', 'style' => 'background-color:#FFF'), 'data' => $now, 'format' => 'dd-MM-yyyy  hh:mm'))
            ->add('tags',TextType::class,array('label' => 'Tags', 'attr' => array('class' => 'form-control')))
            ->add('client', EntityType::class, array('class' => 'AppBundle:Clients','choice_label' => 'name',
                'label' => 'Cliente', 'attr' => array('class' => 'form-control')))
            ->add('billable',CheckboxType::class,array('label' => 'Fatturabile','label_attr' => array('class' => 'col-sm-2','style' => 'margin-top:13px'), 'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Salva', 'attr' => array('class' => 'btn btn-success')))
            ->getForm();

        return $this->render('default/timetracker.html.twig', [
            'page_name' => 'Time Tracker',
            'modal_title' => 'Avvia Timer',
            'items' => $items,
            'form' => $form->createView(),
            'functions' => $this
        ]);
    }

    /**
     * @Route("timetracker/start", name="timetrackerStart")
     */
    public function timetrackerStartAjaxAction(Request $request) {
        $now = new \DateTime();
        $tracker = new TimeTracker;
        $data = $request->request->get('form');
        $em = $this->getDoctrine()->getManager();
        $tracker->setTimeStart($now);
        $tracker->setTimeStop(null);
        $tracker->setClient($data["client"]);
        var_dump($data);
        //$tracker->setBillable($data["billable"]);
        $tracker->setTags($data["tags"]);
        $em->persist($tracker);
        $em->flush();


        $response = new RedirectResponse('timetracker');
        return $response;
    }

    /**
     * @Route("/clienti", name="clients")
     */
    public function clientsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository(Clients::class)->findAll();

        $status = false;

        $form = $this->createFormBuilder(new Clients())
            ->add('name', TextType::class, array('label' => 'Ragione Sociale','attr' => array('class' => 'form-control')))
            ->add('vat_number', TextType::class, array('label' => 'Partita IVA','attr' => array('class' => 'form-control')))
            ->add('address', TextType::class, array('label' => 'Indirizzo','attr' => array('class' => 'form-control')))
            ->add('cap', TextType::class, array('label' => 'CAP','attr' => array('class' => 'form-control')))
            ->add('city', TextType::class, array('label' => 'Città','attr' => array('class' => 'form-control')))
            ->add('prov', TextType::class, array('label' => 'Provincia','attr' => array('class' => 'form-control')))
            ->add('tel', TextType::class, array('label' => 'Telefono','attr' => array('class' => 'form-control')))
            ->add('email', TextType::class, array('label' => 'E-Mail','attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Aggiungi', 'attr' => array('class' => 'btn btn-success')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $clients = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($clients);
            $em->flush();
            $status = true;
            return $this->redirectToRoute($request->get('_route'), $request->query->all());
        }

        return $this->render('default/clients.html.twig', [
            'clients' => $clients,
            'form' => $form->createView(),
            'status' => $status,
            'page_name' => 'Clienti',
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/clienti/{id}", name="clients_details")
     */
    public function clientsDetailsAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository(Clients::class)->find($id);

        $clientName = $this->convertClientID($id);

        $invoices = $this->getDoctrine()->getManager()->getRepository(Invoices::class)->findBy(array('client' => $id));

        return $this->render('default/clients.details.html.twig', [
            'page_name' => 'Cliente > ' . $clientName,
            'client' => $clients,
            'invoices' => $invoices,
            'functions' => $this,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/clienti/modifica/{id}", name="clients_edit")
     */
    public function clientsEditAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository(Clients::class)->find($id);

        $clientName = $this->convertClientID($id);

        $form = $this->createFormBuilder( new Clients())
            ->add('name', TextType::class, array('label' => 'Ragione Sociale','attr' => array('class' => 'form-control'), 'data' => $clients->getName()))
            ->add('vat_number', TextType::class, array('label' => 'Partita IVA','attr' => array('class' => 'form-control'), 'data' => $clients->getVatNumber()))
            ->add('address', TextType::class, array('label' => 'Indirizzo','attr' => array('class' => 'form-control'),'data' => $clients->getAddress()))
            ->add('cap', TextType::class, array('label' => 'CAP','attr' => array('class' => 'form-control'), 'data' => $clients->getCap()))
            ->add('city', TextType::class, array('label' => 'Città','attr' => array('class' => 'form-control'),'data' => $clients->getCity()))
            ->add('prov', TextType::class, array('label' => 'Provincia','attr' => array('class' => 'form-control'),'data' => $clients->getProv()))
            ->add('tel', TextType::class, array('label' => 'Telefono','attr' => array('class' => 'form-control'),'data' => $clients->getTel()))
            ->add('email', TextType::class, array('label' => 'E-Mail','attr' => array('class' => 'form-control'),'data' => $clients->getEmail()))
            ->add('save', SubmitType::class, array('label' => 'Salva', 'attr' => array('class' => 'btn btn-success','style'=>'margin-top:-50px')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $clients = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $clients->setName($form->get('name')->getData());
            $clients->setVatNumber($form->get('vat_number')->getData());
            $clients->setAddress($form->get('address')->getData());
            $clients->setCap($form->get('cap')->getData());
            $clients->setCity($form->get('city')->getData());
            $clients->setProv($form->get('prov')->getData());
            $clients->setTel($form->get('tel')->getData());
            $clients->setEmail($form->get('email')->getData());
            $em->flush();
            return $this->redirectToRoute('clients');
        }

        return $this->render('default/clients.edit.html.twig', [
            'page_name' => 'Cliente > ' . $clientName,
            'client' => $clients,
            'form' => $form->createView(),
            'functions' => $this,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/fatture/", name="invoices")
     */
    public function InvoicesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager()->getRepository(Invoices::class);
        $invoices = $em->findAll();

        //$newId = ($invoices->getId() + 1) . '/' . date("Y");

        $status = false;

        $form = $this->createFormBuilder(new Invoices())
            ->add('date', DateType::class, array('widget' => 'single_text','label' => 'Data','attr' => array('class' => 'form-control')))
            ->add('client', EntityType::class, array('class' => 'AppBundle:Clients','choice_label' => 'name',
                'label' => 'Cliente', 'attr' => array('class' => 'form-control')))
            ->add('status', ChoiceType::class, array('label' => 'Stato','attr' => array('class' => 'form-control'), 'choices' => (
                array('Emessa'=>'0', 'Pagata' => '1', 'Inviata' => '2', 'Scaduta' => '3', 'Annullata' => '4'))))
            ->add('total', TextType::class, array('label' => 'Totale','attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Aggiungi', 'attr' => array('class' => 'btn btn-success')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $invoices = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($invoices);
            $em->flush();
            $status = true;
            return $this->redirectToRoute($request->get('_route'), $request->query->all());
        }

        return $this->render('default/invoices.html.twig', [
            'page_name' => 'Fatture',
            'invoices' => $invoices,
            'status' => $status,
            //'newId' => $newId,
            'functions' => $this,
            'form' => $form->createView(),
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/fatture/{id}", name="show_invoice")
    */
    public function showInvoiceAction(Request $request, $id) {

        $settingsFile = $this->getParameter('kernel.project_dir'). DIRECTORY_SEPARATOR .'app/config/app.yml';
        try {
            $settings = Yaml::parse(file_get_contents($settingsFile));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
            $settings = null;
        }

        $invoice = $this->getDoctrine()->getManager()->getRepository(Invoices::class)->find($id);

        $client = $this->getDoctrine()->getManager()->getRepository(Clients::class)->find($invoice->getClient());

        $total = $invoice->getTotal();

        $contribute = number_format(($total * 4) /100,2);
        $grandtotal = number_format($total + $contribute + 2, 2);

        $clientName = $client->getName();
        $status = $this->convertStatus($invoice->getStatus());

        return $this->render('default/invoices.view.html.twig', [
            'invoice' => $invoice,
            'settings' => $settings,
            'client'  => $client,
            'status'  => $status,
            'contribute' => $contribute,
            'grandtotal' => $grandtotal,
            'page_name' => 'Fattura per ' . $clientName
        ]);
    }

    /**
     * @Route("invoices/show/{id}", name="render_invoice")
     */
    public function renderInvoiceAction(Request $request, $id, $invoice, $client, $contribute, $grandtotal, $settings) {

        $html = $this->render('/invoice/default/invoice.html.twig', [
            'invoice' => $invoice,
            'settings' => $settings,
            'client'  => $client,
            'grandtotal' => $grandtotal,
            'contribute' => $contribute,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR
        ]);
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', sprintf('ft.-%s.pdf',$id)),
            ]
        );
    }

    /**
     * @Route("prodotti/", name="products")
     */
    public function productsAction(Request $request) {

        $repo = $this->getDoctrine()->getManager()->getRepository(Products::class);
        $products = $repo->findAll();

        $form = $this->createFormBuilder(new Products())
            ->add('type', ChoiceType::class, array('label' => 'Tipo','attr' => array('class' => 'form-control'),'choices' => array('Prezzo Fisso' => 1, 'Prezzo Orario' => 2)))
            ->add('name', TextType::class, array('label' => 'Nome Prodotto','attr' => array('class' => 'form-control')))
            ->add('price', TextType::class, array('label' => 'Prezzo','attr' => array('class' => 'form-control')))
            ->add('billable', CheckboxType::class, array('label' => 'Fatturabile','required' => true, 'attr' => array('class' => 'form-control')))
            ->add('enabled', CheckboxType::class, array('label' => 'Abilitato', 'required' => false, 'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Aggiungi', 'attr' => array('class' => 'btn btn-success')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($products);
            $em->flush();
            $status = true;
            return $this->redirectToRoute($request->get('_route'), $request->query->all());
        }

        return $this->render('default/products.html.twig', [
            'page_name' => 'Prodotti',
            'modal_title' => 'Aggiungi Prodotto',
            'products' => $products,
            'functions' => $this,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("prodotti/edit/{id}", name="products_edit")
     */
    public function productsModifyAction(Request $request,$id) {

        $repo = $this->getDoctrine()->getManager()->getRepository(Products::class);
        $products = $repo->find($id);

        $editForm = $this->createFormBuilder(new Products())
            ->add('type', ChoiceType::class, array('label' => 'Tipo','attr' => array('class' => 'form-control'),'choices' => array('Prezzo Fisso' => 1, 'Prezzo Orario' => 2),'data' => $products->getType()))
            ->add('name', TextType::class, array('label' => 'Nome Prodotto','attr' => array('class' => 'form-control'),'data' => $products->getName()))
            ->add('price', TextType::class, array('label' => 'Prezzo','attr' => array('class' => 'form-control'), 'data' => $products->getPrice()))
            ->add('billable', CheckboxType::class, array('label' => 'Fatturabile','required' => true, 'attr' => array('class' => 'form-control'), 'data' => $products->getBillable()))
            ->add('enabled', CheckboxType::class, array('label' => 'Abilitato', 'required' => false, 'attr' => array('class' => 'form-control'), 'data' => $products->getEnabled()))
            ->add('save', SubmitType::class, array('label' => 'Modifica', 'attr' => array('class' => 'btn btn-success')))
            ->getForm();

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $status = true;
            return $this->redirectToRoute('products');
        }

        return $this->render('modals/products.add.html.twig', [
            'page_name' => 'Prodotti',
            'modal_title' => 'Modifica Prodotto',
            'products' => $products,
            'functions' => $this,
            'form' => $editForm->createView()
        ]);
    }

    /**
     * @Route("calendario/", name="calendar")
     */
    public function deadlineAction() {
        return $this->render('default/calendar.html.twig',
            ['page_name' => 'Calendario']);
    }

    /**
     * @Route("spese/{type}/{sort}", name="expenses", defaults={"type": null,"sort": null})
     */
    public function expensesAction(Request $request,$type,$sort) {

        $repo = $this->getDoctrine()->getManager()->getRepository(Expenses::class);
        if ($sort == null) {
            $expenses = $repo->findAll();
            $total = $repo->createQueryBuilder('e')->select('SUM(e.total) as totals')->getQuery()->getOneOrNullResult();
        } elseif ($type == 'categoria' && $sort != null) {
            $expenses = $repo->findBy(array('category' => $sort));
            $total = $repo->createQueryBuilder('e')->select('SUM(e.total) as totals')->where('e.category = :sort')->setParameter(':sort', $sort)->getQuery()->getOneOrNullResult();
        } elseif ($type == 'fornitore') {
            $expenses = $repo->findBy(array('supplier' => $sort));
            $total = $repo->createQueryBuilder('e')->select('SUM(e.total) as totals')->where('e.supplier = :sort')->setParameter(':sort', $sort)->getQuery()->getOneOrNullResult();
        } elseif ($type == 'stato') {
            $expenses = $repo->findBy(array('paid' => $sort));
            $total = $repo->createQueryBuilder('e')->select('SUM(e.total) as totals')->where('e.paid = :sort')->setParameter(':sort', $sort)->getQuery()->getOneOrNullResult();
        }
        $form = $this->createFormBuilder(new Expenses())
            ->add('dateReceived', DateType::class, array('widget' => 'single_text','label' => 'Data Ricezione','attr' => array('class' => 'form-control')))
            ->add('supplier', TextType::class, array('label' => 'Fornitore','attr' => array('class' => 'form-control')))
            ->add('total', TextType::class, array('label' => 'Totale','attr' => array('class' => 'form-control')))
            ->add('paid', CheckboxType::class, array('label' => 'Pagato','required' => false, 'attr' => array('class' => 'form-control form-inline')))
            ->add('category', TextType::class, array('label' => 'Categoria', 'attr' => array('class' => 'form-control')))
            ->add('deadline', DateType::class, array('widget' => 'single_text','label' => 'Scadenza', 'required' => true, 'attr' => array('class' => 'form-control')))
            ->add('total', TextType::class, array('label' => 'Totale', 'required' => true, 'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Aggiungi', 'attr' => array('class' => 'btn btn-success')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $expensesForm = $form->getData();
            $em = $this->getDoctrine()->getManager();
            //$expenses->setDateReceived(new \DateTime($expensesForm->get('form_dateReceived')->getData()));
            $em->persist($expensesForm);
            $em->flush();
            $status = true;
            return $this->redirectToRoute($request->get('_route'), $request->query->all());
        }

        $category = $repo->createQueryBuilder('e')->select('e.category')->distinct('e.category')->orderBy('e.category','ASC')->getQuery()->getResult();
        $supplier = $repo->createQueryBuilder('e')->select('e.supplier')->distinct('e.supplier')->orderBy('e.supplier','ASC')->getQuery()->getResult();

        return $this->render('default/expenses.html.twig', [
            'page_name' => 'Spese',
            'expenses' => $expenses,
            'category' => $category,
            'supplier' => $supplier,
            'total' => $total,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("impostazioni/", name="settings")
     */
    public function settingsAction() {

        $settingsFile = $this->getParameter('kernel.project_dir'). DIRECTORY_SEPARATOR .'app/config/app.yml';
        try {
            $value = Yaml::parse(file_get_contents($settingsFile));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
            $value = null;
        }

        return $this->render('default/settings.html.twig',
            ['page_name' => 'Impostazioni',
                'yml' => $value]);
    }

    /**
     * @Route("note/", name="notes")
     */
    public function notesAction() {

        $repo = $this->getDoctrine()->getManager()->getRepository(Notes::class);

        $notes = $repo->findAll();

        return $this->render('default/notes.html.twig',
            ['page_name' => 'Note',
                'notes' => $notes]);
    }

    function idToName($id)
    {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository(Clients::class)->find($id);
    }

    public function convertClientID($id) {
        $repo = $this->getDoctrine()->getManager()->getRepository(Clients::class)->find($id);
        return $repo->getName();
    }

    public function convertStatus($status) {
        switch ($status) {
            case 0:
                return 'Emessa';
                break;
            case 1:
                return 'Pagata';
                break;
            case 2:
                return 'Inviata';
                break;
            case 3:
                return 'Scaduta';
                break;
            case 4:
                return 'Annullata';
                break;
            case 5:
                return 'Posticipata';
                break;
            default:
                return 'Sconosciuto';
                break;
        }
    }

    public function convertProductType($type) {
        switch ($type) {
            case 1:
                return 'Prezzo Fisso';
                break;
            case 2:
                return 'Prezzo Orario';
                break;
            default:
                return 'Sconosciuto';
                break;
        }
    }

    public function decodeJSON($json) {
        return json_decode($json);
    }

    public function getDataByMonth($repo, $year, $month,$type = null)
    {
        $date = new \DateTime("{$year}-{$month}-01 00:00");
        $dateEnd = new \DateTime("{$year}-{$month}-31 23:59");

        $qb = $repo ->createQueryBuilder('b');
        if (!$type) {
            $query = $qb->where('b.date BETWEEN :start AND :end')
                ->setParameter('start', $date->format('Y-m-d H:i:s'))
                ->setParameter('end', $dateEnd->format('Y-m-d H:i:s'))
                ->select('SUM(b.total) as totals');
            /*$query = $qb->where('MONTH(b.date) = :month')
                ->setParameter('month', $month)
                ->select('SUM(b.total) as totals');*/
        } elseif ($type=1) {
            $query = $qb->where('b.date BETWEEN :start AND :end')
                ->setParameter('start', $date->format('Y-m-d H:i:s'))
                ->setParameter('end', $dateEnd->format('Y-m-d H:i:s'))
                ->andWhere('b.status = :status')
                ->setParameter('status', '1')
                ->select('SUM(b.total) as totals');
        }
        $result = $query->getQuery()->getSingleScalarResult();
        if ($result == NULL) { return '0'; } else { return $result; }
    }

    public function getExpenseByMonth($repo, $year, $month,$type = null)
    {
        $date = new \DateTime("{$year}-{$month}-01 00:00");
        $dateEnd = new \DateTime("{$year}-{$month}-31 23:59");

        $qb = $repo ->createQueryBuilder('b');
        if (!$type) {
            $query = $qb->where('b.dateReceived BETWEEN :start AND :end')
                ->setParameter('start', $date->format('Y-m-d H:i:s'))
                ->setParameter('end', $dateEnd->format('Y-m-d H:i:s'))
                ->select('SUM(b.total) as totals');
            /*$query = $qb->where('MONTH(b.date) = :month')
                ->setParameter('month', $month)
                ->select('SUM(b.total) as totals');*/
        } elseif ($type=1) {
            $query = $qb->where('b.dateReceived BETWEEN :start AND :end')
                ->setParameter('start', $date->format('Y-m-d H:i:s'))
                ->setParameter('end', $dateEnd->format('Y-m-d H:i:s'))
                ->andWhere('b.paid = :paid')
                ->setParameter('paid', '1')
                ->select('SUM(b.total) as totals');
        }
        $result = $query->getQuery()->getSingleScalarResult();
        if ($result == NULL) { return '0'; } else { return $result; }
    }

}


