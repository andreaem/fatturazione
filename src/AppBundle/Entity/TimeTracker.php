<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TimeTracker
 *
 * @ORM\Table(name="time_tracker")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TimeTrackerRepository")
 */
class TimeTracker
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeStart", type="datetime")
     */
    private $timeStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeStop", type="datetime", nullable=true)
     */
    private $timeStop;

    /**
     * @var array
     *
     * @ORM\Column(name="tags", type="json_array")
     */
    private $tags;

    /**
     * @var int
     *
     * @ORM\Column(name="client", type="integer")
     */
    private $client;

    /**
     * @var bool
     *
     * @ORM\Column(name="billable", type="boolean", nullable=true)
     */
    private $billable;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeStart
     *
     * @param \DateTime $timeStart
     *
     * @return TimeTracker
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeStop
     *
     * @param \DateTime $timeStop
     *
     * @return TimeTracker
     */
    public function setTimeStop($timeStop)
    {
        $this->timeStop = $timeStop;

        return $this;
    }

    /**
     * Get timeStop
     *
     * @return \DateTime
     */
    public function getTimeStop()
    {
        return $this->timeStop;
    }

    /**
     * Set tags
     *
     * @param array $tags
     *
     * @return TimeTracker
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set client
     *
     * @param integer $client
     *
     * @return TimeTracker
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return int
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set billable
     *
     * @param boolean $billable
     *
     * @return TimeTracker
     */
    public function setBillable($billable)
    {
        $this->billable = $billable;

        return $this;
    }

    /**
     * Get billable
     *
     * @return bool
     */
    public function getBillable()
    {
        return $this->billable;
    }
}

